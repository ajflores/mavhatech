import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { WorkTaskComponent } from './work-task/work-task.component';
import { WorkTaskdataService } from './work-task/work-taskdata.service';
import { Ng2TableModule } from 'ng2-table/ng2-table';
import { WorkTaskEditComponent } from './work-task/work-task-edit.component';
import { NgxSpinnerModule } from "ngx-spinner";


@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    WorkTaskComponent,
    WorkTaskEditComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
      Ng2TableModule,
      NgxSpinnerModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'counter', component: CounterComponent },
      { path: 'fetch-data', component: FetchDataComponent },
      { path: 'workTask', component: WorkTaskComponent },
        { path: 'workTask/edit/:id', component: WorkTaskEditComponent },
        { path: 'workTask/edit', component: WorkTaskEditComponent },
    ])
  ],
  providers: [WorkTaskdataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
