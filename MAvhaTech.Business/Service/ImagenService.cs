﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MAvhaTech.Business.Entities;
using Microsoft.AspNetCore.Http;

namespace MAvhaTech.Business.Service
{
    public class ImagenService : IImagenService
    {
        public Imagen CreatePicture(IFormFile file)
        {
            if (file == null)
                throw new Exception("File is null");
            if (file.Length == 0)
                throw new Exception("File is empty");

            var folderName = Path.Combine("Resources", "ProfilePics");
            String path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            String filePath = Path.Combine(path, DateTime.Now.Year.ToString());

            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);

            var uniqueFileName = $"{DateTime.Now.Ticks}_profilepic.png";
            var fullPath = Path.Combine(filePath, uniqueFileName);
            try
            {
                using (var fileStream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }
                return new Imagen() { Path = fullPath, Name = uniqueFileName };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
