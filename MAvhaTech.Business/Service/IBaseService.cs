﻿using MAvhaTech.Business.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MAvhaTech.Business.Service
{
    public interface IBaseService<T> where T : Entity
    {
        bool Insert(T entidad);
        bool Update(T entidad);
        bool Delete(T entidad);
        T GetById(int Id);
        IList<T> GetAll();
        bool SaveUpdate(T entidad);
    }
}
