﻿using MAvhaTech.Business.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace MAvhaTech.Business.Service
{
    public interface IImagenService
    {
        Imagen CreatePicture(IFormFile file);
    }
}
