﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using NHibernate.Linq;
using MAvhaTech.Business.Repositories;
using MAvhaTech.Data.EF;
using MAvhaTech.Business.Entities;

namespace MAvhaTech.Data.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : Entity
    {
        protected readonly MavhaTechDbContext _context;
        public BaseRepository(MavhaTechDbContext context)
        {
            this._context = context;
        }
        protected void Save() => _context.SaveChanges();
        public bool Delete(T entidad)
        {
            entidad.Deleted = true;
            this.Update(entidad);
            return true;
        }

        public IList<T> GetAll()
        {
            return _context.Set<T>().ToList();
        }

        public T GetById(int id)
        {
            return _context.Set<T>().Where(t => t.Id == id).FirstOrDefault();
        }

        public bool Insert(T entidad)
        {
            _context.Add(entidad);
            Save();
            return true;
        }

        public bool Update(T entidad)
        {
            _context.Entry(entidad).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            Save();
            return true;
        }

        public bool SaveUpdate(T entidad)
        {
            if (entidad.Id == 0)
                return this.Insert(entidad);
            else
                return this.Update(entidad);
        }
    }
}
