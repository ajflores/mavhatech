﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAvhaTech.Business.Entities
{
    public abstract class Entity : IEquatable<Entity>
    {
        public int Id { get; set; }
        public bool Deleted { get; set; }
        #region IEquatable
        public bool Equals(Entity other)
        {
            if (Id == other.Id)
                return true;

            return false;
        }
        #endregion
    }
}
