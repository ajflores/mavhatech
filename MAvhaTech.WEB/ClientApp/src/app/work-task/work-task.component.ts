import { Component } from '@angular/core';
import { WorkTaskdataService } from './work-taskdata.service';
import { IWorkTask } from '../interface/IWorkTask';
import { TypeFilter } from '../service/typeFilter';
import { Router } from '@angular/router';
@Component({
    selector: 'app-work-task',
    templateUrl: './work-task.component.html',
})
/** WorkTask component*/
export class WorkTaskComponent {
    worktasks: IWorkTask[];
    value: any;
    isFilter: boolean = false;
  textFilter: string = "";
  typeFilters = TypeFilter;
  selectedTypeFilter: any = TypeFilter.All;
  selectedOption: any = [{ id: TypeFilter.All, value: TypeFilter[TypeFilter.All] }];
  public options = [
    { id: TypeFilter.All,           value: TypeFilter[TypeFilter.All] },
    { id: TypeFilter.ById,          value: TypeFilter[TypeFilter.ById] },
    { id: TypeFilter.ByDescripcion, value: TypeFilter[TypeFilter.ByDescripcion] },
    { id: TypeFilter.ByEstado,      value: TypeFilter[TypeFilter.ByEstado] },
    ]
    /** WorkTask ctor */
    constructor(private dataService: WorkTaskdataService, private router: Router) {
    }
    ngOnInit(): void {
    }
  consultar_click(): void {
    if (this.selectedOption.id == 0)
      this.textFilter = "All";

    this.loadWorkTask();
    }
    private loadWorkTask() {
        this.dataService.getAllTypeFilter(this.selectedOption.value, this.textFilter).subscribe((res => {
            this.worktasks = res;
        }));
    }

    edit_click(id: number): void {
        this.router.navigate(['workTask/edit/' + id]);
    }
    remove_click(id: number): void {
        var d = this.worktasks.find(x => x.id == id);
        d.deleted = true;
        this.dataService.saveUpdate(d).subscribe(res => {
            this.loadWorkTask();
        });
    }
    agregar_click(): void {
        this.router.navigate(['workTask/edit/']);
    }
  value_change(event: any): void {
      this.selectedOption = event;
    }
}
