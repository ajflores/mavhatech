﻿using Autofac;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MAvhaTech.Business.Repositories;
using MAvhaTech.Data.Repositories;
using MAvhaTech.Business.Service;


namespace MAvhaTech.WEB.AutofacConfig
{
    public class AutofacModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // The generic ILogger<TCategoryName> service was added to the ServiceCollection by ASP.NET Core.
            // It was then registered with Autofac using the Populate method. All of this starts
            // with the services.AddAutofac() that happens in Program and registers Autofac
            // as the service provider.
            //builder.RegisterType<WorkTaskRepository>().InstancePerRequest();

            //builder.Register(c => new WorkTaskService(c.Resolve<WorkTaskRepository>()))
            //    .As<IWorkTaskService>()
            //    .InstancePerLifetimeScope();

            var repoAssembly = System.Reflection.Assembly.GetAssembly(typeof(WorkTaskRepository));
            builder.RegisterAssemblyTypes(repoAssembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .PropertiesAutowired();

            var serviceAssembly = System.Reflection.Assembly.GetAssembly(typeof(WorkTaskService));
            builder.RegisterAssemblyTypes(serviceAssembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .PropertiesAutowired();
        }
    }
}
