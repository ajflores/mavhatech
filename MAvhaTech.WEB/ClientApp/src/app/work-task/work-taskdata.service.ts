import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { Observable } from 'rxjs';
import { IWorkTask } from '../interface/IWorkTask';
import { catchError, map } from 'rxjs/operators';
import { TypeFilter } from '../service/typeFilter';
import { IImagen } from '../interface/IImagen';

@Injectable()
export class WorkTaskdataService {

  _baseUrl: string = '';

  constructor(private http: HttpClient) {
    this._baseUrl = 'http://localhost:64140/api/';
  }

  getAllWorkTask(): Observable<IWorkTask[]> {
    return this.http.get<IWorkTask[]>(this._baseUrl + "workTask").pipe(
      map(res => {
        return res;
      }));
  }
  getAllTypeFilter(filter: TypeFilter, txtFilter: string): Observable<IWorkTask[]> {
    return this.http.get<IWorkTask[]>(this._baseUrl + "workTask/GetAllFilter/" + filter + "/" + txtFilter).pipe(
      map(res => {
        return res;
      }));
  }
  getWorkTask(id: number): Observable<IWorkTask> {
    return this.http.get<IWorkTask>(this._baseUrl + "workTask/" + id).pipe(
      map(res => {
        return res;
      }));
  }
  saveUpdate(workTask: IWorkTask): Observable<void> {
    let headers: HttpHeaders = new HttpHeaders().set("Content-Type", "application/json");
    return this.http.post(this._baseUrl + "workTask/", JSON.stringify(workTask), {
      headers: headers
    }).pipe(
      map(() => {

      }),);
  }
    saveFileRequest(file: File): Observable<IImagen> {
    let input = new FormData();
    input.append("file", file);
    let headers: HttpHeaders = new HttpHeaders().set("Content-Type", "application/json");
        return this.http.post<IImagen>(this._baseUrl + "workTask/FileRequest/", input, { observe: "response" }).pipe(
            map((res: HttpResponse<string>) => {
          if (res.status == 200)
            return res.body;
          else
            return undefined;
        }));
    }

}
