import { Component } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { IWorkTask } from '../interface/IWorkTask';
import { WorkTaskdataService } from './work-taskdata.service';
@Component({
    selector: 'app-work-task-edit',
    templateUrl: './work-task-edit.component.html',
})
/** workTaskEdit component*/
export class WorkTaskEditComponent {
  id: number = 0;
  fileToUpload: File = null;
  worktask: IWorkTask;
  loading: boolean = false;

  /** workTaskEdit ctor */
  constructor(private dataService: WorkTaskdataService,
    private route: ActivatedRoute) {

  }
  ngOnInit(): void {
    if (this.route.snapshot.params["id"] == null || this.route.snapshot.params["id"] == undefined) {
      this.worktask = { id: 0, estado: false, descripcion: "", deleted: false, path: "" };
      this.loading = true;
    }
    else {
      this.id = +this.route.snapshot.params["id"];
      this.loadWorkTask(this.id);
    }
  }
  loadWorkTask(id: number) {
    this.dataService.getWorkTask(id).subscribe((res => {
      this.worktask = res;
      
      this.loading = true;
    }));
    }
  handleFileInput(files: FileList): void {
      this.fileToUpload = files[0];
      this.worktask.path = this.fileToUpload.name;
  }
  removeImagen_click(): void {
    if (this.worktask) {
      this.worktask.path = "";
      this.fileToUpload = null;
    }
  }
    saveUpdate_click(): void {
    // primero save pictur si existe;
    if (this.fileToUpload) {
        this.dataService.saveFileRequest(this.fileToUpload).subscribe(res => {
            if (res)
                this.worktask.path = res.path;
            else
                this.worktask.path = "";
        this.dataService.saveUpdate(this.worktask).subscribe(res => {
            window.history.back();
        });
      });
    }
    else {
      this.dataService.saveUpdate(this.worktask).subscribe(res => {
          window.history.back();
      });
    }
    
  }
}
