﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace MAvhaTech.Business.Entities
{
    public class WorkTask: Entity
    {
        public string Descripcion { get; set; }
        public bool Estado { get; set; }
        public string Path { get; set; }
    }
}
