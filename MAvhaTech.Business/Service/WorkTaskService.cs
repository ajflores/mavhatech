﻿using MAvhaTech.Business.Entities;
using MAvhaTech.Business.Repositories;
using MAvhaTech.Business.Tools;
using System;
using System.Collections.Generic;
using System.Text;

namespace MAvhaTech.Business.Service
{
    public class WorkTaskService: BaseService<WorkTask>, IWorkTaskService
    {
        private readonly IWorkTaskRepository workTaskRepository;
        public WorkTaskService(IBaseRepository<WorkTask> repoFactory) :base(repoFactory)
        {
            this.workTaskRepository = (IWorkTaskRepository)repoFactory;
        }

        public IList<WorkTask> FindWithFilter(TypeFilter filter, string txtfilter)
        {
            switch (filter)
            {
                case TypeFilter.All:
                    return workTaskRepository.FindWithFilter(x => x.Deleted == false);
                case TypeFilter.ById:
                    int.TryParse(txtfilter,out int value);
                    return workTaskRepository.FindWithFilter(x => x.Deleted == false && x.Id == value);
                case TypeFilter.ByDescripcion:
                    return workTaskRepository.FindWithFilter(x => x.Deleted == false && x.Descripcion == txtfilter);
                case TypeFilter.ByEstado:
                    return workTaskRepository.FindWithFilter(x => x.Deleted == false && x.Estado == BoolParser.GetValue(txtfilter));
                case TypeFilter.none:
                    return workTaskRepository.FindWithFilter(x => x.Deleted == false);
                default:
                    return new List<WorkTask>();
            }
        }
    }
}
