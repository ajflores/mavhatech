﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAvhaTech.Business.Entities
{
    public class Imagen
    {
        public string Name { get; set; }
        public string Path { get; set; }
    }
}
