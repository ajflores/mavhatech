export interface IWorkTask {
  id: number;
  descripcion: string;
  estado: boolean;
  path: string;
  deleted: boolean;
}
