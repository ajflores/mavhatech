﻿using MAvhaTech.Business.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace MAvhaTech.Data.EF
{
    public class MavhaTechDbContext : DbContext
    {
        public MavhaTechDbContext(DbContextOptions<MavhaTechDbContext> options): base(options)
        {

        }
        public DbSet<WorkTask> WorkTasks { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
