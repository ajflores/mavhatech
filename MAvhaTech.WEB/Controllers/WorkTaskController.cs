﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MAvhaTech.Business.Entities;
using MAvhaTech.Business.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MAvhaTech.WEB.Controllers
{
    [Route("api/[controller]")]
    public class WorkTaskController : Controller
    {
        private readonly IWorkTaskService workTaskService;
        private readonly IImagenService imagenService;
        public WorkTaskController(IWorkTaskService workTaskService, IImagenService imagenService)
        {
            this.workTaskService = workTaskService;
            this.imagenService = imagenService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var lista = this.workTaskService.GetAll();
            return Ok(lista);
        }

        [HttpGet("{id}")]
        public IActionResult GetAll(int id)
        {
            var lista = this.workTaskService.GetById(id);
            return Ok(lista);
        }

        [HttpGet("GetAllFilter/{filter}/{txtfilter}")]
        public IActionResult GetAllFilter(TypeFilter filter, string txtfilter)
        {
            var lista = this.workTaskService.FindWithFilter(filter, txtfilter);
            return Ok(lista);
        }
        [HttpPost]
        public IActionResult SaveUpdate([FromBody]WorkTask workTask)
        {
            this.workTaskService.SaveUpdate(workTask);
            return Ok();
        }

        [HttpPost("FileRequest")]
        public IActionResult FileRequest(IFormFile file)
        {
            var result = imagenService.CreatePicture(file);
            return Ok(result);
        }
        //GET api/file/id
        [HttpGet("GetFile/{id}")]
        public IActionResult GetFile(int id)
        {
            var wt = workTaskService.GetById(id);
            if (wt == null)
                return Ok(null);
            var imageFileStream = System.IO.File.OpenRead(wt.Path);
            return File(imageFileStream, "image/jpeg");
        }
    }
}