﻿using MAvhaTech.Business.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MAvhaTech.Business.Service
{
    public interface IWorkTaskService : IBaseService<WorkTask>
    {
        IList<WorkTask> FindWithFilter(TypeFilter filter, string txtfilter);
    }
}
