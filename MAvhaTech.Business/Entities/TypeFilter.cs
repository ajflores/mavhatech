﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAvhaTech.Business.Entities
{
    public enum TypeFilter
    {
        All,
        ById,
        ByDescripcion,
        ByEstado,
        none
    }
}
