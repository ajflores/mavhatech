﻿using MAvhaTech.Business.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MAvhaTech.Business.Repositories
{
    public interface IWorkTaskRepository
    {
        IList<WorkTask> FindWithFilter(Func<WorkTask, bool> predicate);
    }
}
