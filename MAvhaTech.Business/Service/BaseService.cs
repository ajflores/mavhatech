﻿using MAvhaTech.Business.Entities;
using MAvhaTech.Business.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace MAvhaTech.Business.Service
{
    public class BaseService<T> where T :Entity
    {
        private readonly IBaseRepository<T> repoFactory;
        public BaseService(IBaseRepository<T> repoFactory)
        {
            this.repoFactory = repoFactory;
        }
        public bool Delete(T entidad)
        {
            return repoFactory.Delete(entidad);
        }

        public IList<T> GetAll()
        {
            return repoFactory.GetAll();
        }

        public T GetById(int Id)
        {
            return repoFactory.GetById(Id);
        }

        public bool Insert(T entidad)
        {
            return repoFactory.Insert(entidad);
        }

        public bool Update(T entidad)
        {
            return repoFactory.Update(entidad);
        }
        public bool SaveUpdate(T entidad)
        {
            return repoFactory.SaveUpdate(entidad);
        }
    }
}
