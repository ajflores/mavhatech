"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TypeFilter;
(function (TypeFilter) {
    TypeFilter[TypeFilter["All"] = 0] = "All";
    TypeFilter[TypeFilter["ById"] = 1] = "ById";
    TypeFilter[TypeFilter["ByDescripcion"] = 2] = "ByDescripcion";
    TypeFilter[TypeFilter["ByEstado"] = 3] = "ByEstado";
})(TypeFilter = exports.TypeFilter || (exports.TypeFilter = {}));
//# sourceMappingURL=TypeFilter.js.map