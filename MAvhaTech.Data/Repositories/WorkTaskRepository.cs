﻿using MAvhaTech.Business.Entities;
using MAvhaTech.Business.Repositories;
using MAvhaTech.Data.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MAvhaTech.Data.Repositories
{
    public class WorkTaskRepository : BaseRepository<WorkTask>, IWorkTaskRepository
    {
        public WorkTaskRepository(MavhaTechDbContext context) : base(context)
        {
        }
        public IList<WorkTask> FindWithFilter(Func<WorkTask,bool> predicate)
        {
            return _context.WorkTasks.Where(predicate).ToList();
        }
    }
}
