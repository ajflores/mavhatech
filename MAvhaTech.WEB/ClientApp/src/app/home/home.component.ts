import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit{
    constructor(private spinner: NgxSpinnerService, private router: Router) {

    }
    ngOnInit() {
        debugger;
        this.spinner.show();

        setTimeout(() => {
            /** spinner ends after 5 seconds */
            this.spinner.hide();
            this.router.navigate(['workTask/']);

        }, 5000);
    }
}
