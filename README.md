# MAvhaTech

Welcome to your new single-page application, built with:

ASP.NET Core and C# for cross-platform server-side code
Angular and TypeScript for client-side code
Bootstrap for layout and styling
To help you get started, we've also set up:

Client-side navigation. For example, click Counter then Back to return here.
Angular CLI integration. In development mode, there's no need to run ng serve. It runs in the background automatically, so your client-side resources are dynamically built on demand and the page refreshes when you modify any file.
Efficient production builds. In production mode, development-time features are disabled, and your dotnet publish configuration automatically invokes ng build to produce minified, ahead-of-time compiled JavaScript files.
The ClientApp subdirectory is a standard Angular CLI application. If you open a command prompt in that directory, you can run any ng command (e.g., ng test), or use npm to install extra packages into it.


1- instalar 
```
https://dotnet.microsoft.com/download/dotnet-core/2.2.
```
2- instalar
```
https://nodejs.org/es/ v8.16.1
```
3- instalar
```
https://cli.angular.io/
```
Version npn 6.4.1

Descargar Proyecto
```
git clone https://ajflores@bitbucket.org/ajflores/mavhatech.git
```
Instalar Dependencias
```
npm install en carpeta "mavhatech/MAvhaTech.WEB/ClientApp"
```
Aclaracion.
```
Si el visual studio no emite error ejecutar un tsc (https://www.npmjs.com/package/typescript)
```
Crear Tabla WorkTasks
```
CREATE TABLE MES.dbo.WorkTasks (
  Id int IDENTITY,
  Descripcion varchar(255) NOT NULL,
  Estado bit NOT NULL DEFAULT (0),
  Path varchar(150) NULL,
  Deleted bit NULL DEFAULT (0),
  CONSTRAINT PK_WorkTask_Id PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO
```
Configurar ConnectionStrings
file appSettings.json
```
"ConnectionStrings": {
    "MAVHATECH": "Server=Host;Database=NombreDB;User ID=UserDB;Password=PassDB;MultipleActiveResultSets=false;"
  }
  ```