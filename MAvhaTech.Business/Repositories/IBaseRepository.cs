﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAvhaTech.Business.Repositories
{
    public interface IBaseRepository<T>
    {
        bool Insert(T entidad);
        bool Update(T entidad);
        bool Delete(T entidad);
        T GetById(int Id);
        IList<T> GetAll();
        bool SaveUpdate(T entidad);
    }
}
